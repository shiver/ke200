var http = require("../utils/request")
Page({

  /**
   * 页面的初始数据
   */

  data: {
    userName: '',
    userPwd: ''
  },

  userNameInput: function(e) {
    var that = this
    that.setData({
      userName: e.detail.value
    })
  },

  userPwdInput: function(e) {
    var that = this
    that.setData({
      userPwd: e.detail.value
    })
  },

  wxlogin: function() {
    var param = {
      number:this.data.userName,
      password:this.data.userPwd
    }
    console.log(param)
    http.post("/api/login",JSON.stringify(param)).then(function(res){
      console.log(res)
      if(res.code!=0){
        wx.showModal({
          cancelColor: 'cancelColor',
          title:res.message
        })
        //  'X-Nideshop-Token': wx.getStorageSync('token'),
      }else{
        wx.switchTab({
          url: '../index/index',
        })
        wx.setStorageSync('kc-Token', res.token)
        wx.setStorage({
          data: res,
          key: "user",
        })
      }
    })
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'kc-Token',
      success: function(res) {
        console.log("登陆信息", res)
        if (res.data == null || res.data != null) {
          wx.redirectTo({
            url: '../user/user-center/index',
          })
        } else {
          wx.redirectTo({
            url: '../login/index',
          })
        }
      },
    })
  },
  regist:function(){
    wx.navigateTo({
      url: '../regist/regist',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})