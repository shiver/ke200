package com.kc.api.controller;

import lombok.Data;

@Data
public class LoginModel {
    private Long number;
    private String password;
    private String openid;
}
