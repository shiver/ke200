package com.background.controller;


import com.background.model.User;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.background.common.R;
import java.util.HashMap;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/4/30 19:42
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("system")
public class ApiGetSystemInfoController  {
    @Autowired
    private Environment environment;

    @RequestMapping("/info")
    public R getSystem(){
//        User user= (User) SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> result = new HashMap<>(4);
        result.put("env", environment);
        result.put("user","");
        return R.ok(result);
    }
}
