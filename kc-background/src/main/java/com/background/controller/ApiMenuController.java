package com.background.controller;


import com.background.common.R;
import com.background.model.MenuEntity;
import com.background.model.User;
import com.background.service.MenuService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/3/8 9:48
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sys")
public class ApiMenuController {
    @Autowired
    private MenuService menuService;

    //根据角色获取到菜单
    @RequestMapping("/menu")
    @RequiresPermissions("user:admin:select:add")
    public R SysManager() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        List<MenuEntity> menuEntityList = menuService.getMenuByUserId(user.getUid());
        Map<String, Object> map = new HashMap<>();
        map.put("list", menuEntityList);
        return R.ok(map);
    }
}
