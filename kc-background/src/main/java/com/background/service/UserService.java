package com.background.service;


import com.background.model.MenuEntity;
import com.background.model.User;

import java.util.List;


/**
 * author shish
 * Create Time 2019/1/28 17:20
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface UserService {
    int insertUserInfo(User userInfo);
    User findByUserName(String userName);

    List<MenuEntity> getMenuByUserId(Integer userId);
}
