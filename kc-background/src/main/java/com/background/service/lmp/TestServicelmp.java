package com.background.service.lmp;

import com.background.mapper.TestMapper;
import com.background.model.MenuEntity;
import com.background.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * author shish
 * Create Time 2019/3/7 16:16
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class TestServicelmp implements TestService {
    @Autowired
    private TestMapper testMapper;
    @Override
    public int test(MenuEntity menuEntity) {
        return testMapper.insert( menuEntity);
    }
}
