package com.background.service.lmp;

import com.background.mapper.MenuMapper;
import com.background.mapper.UserMapper;
import com.background.model.MenuEntity;
import com.background.model.User;
import com.background.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/1/12 16:35
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class UserServicelmp implements UserService {
  @Autowired
  private UserMapper userMapper;
  @Autowired
  private MenuMapper menuMapper;
    @Override
    public int insertUserInfo(User userInfo) {
        return userMapper.insert(userInfo);
    }

    @Override
    public User findByUserName(String userName) {
      QueryWrapper<User> queryWrapper=new QueryWrapper<>();
     queryWrapper.eq("username",userName);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public List<MenuEntity> getMenuByUserId(Integer userId) {
        return menuMapper.getMenuByUserId(userId);
    }
}
