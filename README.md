#   时间已经很久了这个版本，目前暂停维护：请移步到这个版本：https://gitee.com/io123/kcplat
# 小程序上课考勤系统，后台管理，小程序前端GPS签到，小程序定位打卡，迟到统计等等

#### 介绍
基于spring boot 开发,上课考勤系统毕业设计，该系统设计获得优秀毕业设计。

#### 软件架构
软件架构说明
  spring boot2.0

#### 安装教程

1.  导入idea
![输入图片说明](doc/image.png)
![输入图片说明](doc/image1.png)
更新代码
![输入图片说明](doc/image2.png)
2.  下载redis
3.  导入mysql数据

#### 使用说明

1. 启动：KcBackgroudApplication 后台管理：http://127.0.0.1:8083
2. 启动：KcApiApplication api接口（对接小程序）：http://127.0.0.1:8080
###  项目交流群
![输入图片说明](doc/code.png)
